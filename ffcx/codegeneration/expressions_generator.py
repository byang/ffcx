# Copyright (C) 2019 Michal Habera
#
# This file is part of FFCX.(https://www.fenicsproject.org)
#
# SPDX-License-Identifier:    LGPL-3.0-or-later

import collections
import logging

import ufl
from ffcx.codegeneration.backend import FFCXBackend
from ffcx.codegeneration.C.format_lines import format_indented_lines
from ffcx.ir.representationutils import initialize_expression_code

logger = logging.getLogger(__name__)


def generate_expression_code(ir, parameters):

    backend = FFCXBackend(ir, parameters)
    eg = ExpressionGenerator(ir, backend)
    code = initialize_expression_code(ir)
    parts = eg.generate()

    body = format_indented_lines(parts.cs_format(), 1)
    code["tabulate_expression"] = body

    code["original_coefficient_positions"] = format_indented_lines(
        eg.generate_original_coefficient_positions().cs_format(), 1)

    code["points"] = format_indented_lines(eg.generate_points().cs_format(), 1)
    code["value_shape"] = format_indented_lines(eg.generate_value_shape().cs_format(), 1)

    return code


class ExpressionGenerator:
    def __init__(self, ir, backend):

        if len(ir.all_num_points) != 1:
            raise RuntimeError("Only one set of points allowed for expression evaluation")

        self.ir = ir
        self.backend = backend
        self.scope = {}
        self._ufl_names = set()
        self.finalization_blocks = collections.defaultdict(list)
        self.symbol_counters = collections.defaultdict(int)
        self.shared_symbols = {}
        self.num_points = self.ir.all_num_points[0]

    def generate(self):
        L = self.backend.language

        parts = []

        parts += self.generate_element_tables()
        parts += self.generate_unstructured_piecewise_partition()

        all_preparts = []
        all_quadparts = []

        preparts, quadparts = self.generate_quadrature_loop()
        all_preparts += preparts
        all_quadparts += quadparts

        preparts, quadparts = self.generate_dofblock_partition(quadrature_independent=True)
        all_preparts += preparts
        all_quadparts += quadparts

        # Collect parts before, during, and after quadrature loops
        parts += all_preparts
        parts += all_quadparts

        return L.StatementList(parts)

    def generate_element_tables(self):
        L = self.backend.language
        parts = []

        tables = self.ir.unique_tables

        alignas = self.ir.params["alignas"]
        padlen = self.ir.params["padlen"]
        table_names = sorted(tables)

        for name in table_names:
            table = tables[name]
            decl = L.ArrayDecl(
                "static const ufc_scalar_t", name, table.shape, table, alignas=alignas, padlen=padlen)
            parts += [decl]

        # Add leading comment if there are any tables
        parts = L.commented_code_list(parts, [
            "Precomputed values of basis functions",
            "FE* dimensions: [entities][points][dofs]",
        ])
        return parts

    def generate_quadrature_loop(self):
        """Generate quadrature loop for this num_points."""
        L = self.backend.language

        # Generate unstructured varying partition
        body = self.generate_unstructured_varying_partition()
        body = L.commented_code_list(
            body, "Points loop body setup (num_points={0})".format(self.num_points))

        # Generate dofblock parts, some of this
        # will be placed before or after quadloop
        preparts, quadparts = \
            self.generate_dofblock_partition()
        body += quadparts

        # Wrap body in loop or scope
        if not body:
            # Could happen for integral with everything zero and optimized away
            quadparts = []
        else:
            iq = self.backend.symbols.quadrature_loop_index()
            quadparts = [L.ForRange(iq, 0, self.num_points, body=body)]

        return preparts, quadparts

    def generate_unstructured_varying_partition(self):
        L = self.backend.language

        # Get annotated graph of factorisation
        F = self.ir.varying_irs[self.num_points]["factorization"]

        arraysymbol = L.Symbol("sv%d" % self.num_points)
        parts = self.generate_partition(arraysymbol, F, "varying", self.num_points)
        parts = L.commented_code_list(parts, "Unstructured varying computations for num_points=%d" %
                                      (self.num_points, ))
        return parts

    def generate_unstructured_piecewise_partition(self):
        L = self.backend.language

        # Get annotated graph of factorisation
        F = self.ir.piecewise_ir["factorization"]

        arraysymbol = L.Symbol("sp")
        num_points = None
        parts = self.generate_partition(arraysymbol, F, "piecewise", num_points)
        parts = L.commented_code_list(parts, "Unstructured piecewise computations")
        return parts

    def generate_dofblock_partition(self, quadrature_independent=False):
        if quadrature_independent is True:  # NB! None meaning piecewise partition, not custom integral
            block_contributions = self.ir.piecewise_ir["block_contributions"]
        else:
            block_contributions = self.ir.varying_irs[self.num_points]["block_contributions"]

        preparts = []
        quadparts = []

        blocks = [(blockmap, blockdata)
                  for blockmap, contributions in sorted(block_contributions.items())
                  for blockdata in contributions]

        for blockmap, blockdata in blocks:

            # Define code for block depending on mode
            block_preparts, block_quadparts = \
                self.generate_block_parts(self.num_points, blockmap, blockdata, quadrature_independent)

            # Add definitions
            preparts.extend(block_preparts)

            # Add computations
            quadparts.extend(block_quadparts)

        return preparts, quadparts

    def generate_block_parts(self, num_points, blockmap, blockdata, quadrature_independent=False):
        """Generate and return code parts for a given block.

        Returns parts occuring before, inside, and after
        the quadrature loop identified by num_points.

        """
        L = self.backend.language

        # The parts to return
        preparts = []
        quadparts = []

        block_rank = len(blockmap)
        blockdims = tuple(len(dofmap) for dofmap in blockmap)

        ttypes = blockdata.ttypes
        if "zeros" in ttypes:
            raise RuntimeError("Not expecting zero arguments to be left in dofblock generation.")

        iq = self.backend.symbols.quadrature_loop_index()

        arg_indices = tuple(self.backend.symbols.argument_loop_index(i) for i in range(block_rank))

        # Get factor expression
        if blockdata.factor_is_piecewise:
            F = self.ir.piecewise_ir["factorization"]
        else:
            F = self.ir.varying_irs[num_points]["factorization"]

        assert not blockdata.transposed, "Not handled yet"
        components = ufl.product(self.ir.expression_shape)

        A_shape = self.ir.tensor_shape
        Asym = self.backend.symbols.element_tensor()
        A = L.FlattenedArray(Asym, dims=[components] + [self.num_points] + A_shape)

        # Prepend dimensions of dofmap block with free index
        # for quadrature points and expression components
        B_indices = tuple([iq] + list(arg_indices))

        # Fetch code to access modified arguments
        # An access to FE table data
        arg_factors = self.get_arg_factors(blockdata, block_rank, num_points, iq, B_indices)

        A_indices = []
        for i in range(len(blockmap)):
            offset = blockmap[i][0]
            A_indices.append(arg_indices[i] + offset)
        A_indices = tuple([iq] + A_indices)

        # Multiply collected factors
        # For each component of the factor expression
        # add result inside quadloop
        body = []

        for fi_ci in blockdata.factor_indices_comp_indices:
            f = self.get_var(num_points, F.nodes[fi_ci[0]]["expression"])
            Brhs = L.float_product([f] + arg_factors)
            body.append(L.AssignAdd(A[(fi_ci[1],) + A_indices], Brhs))

        for i in reversed(range(block_rank)):
            body = L.ForRange(
                B_indices[i + 1], 0, blockdims[i], body=body)
        quadparts += [body]

        return preparts, quadparts

    def get_arg_factors(self, blockdata, block_rank, num_points, iq, indices):
        L = self.backend.language

        arg_factors = []
        for i in range(block_rank):
            mad = blockdata.ma_data[i]
            td = mad.tabledata
            mt = self.ir.piecewise_ir["modified_arguments"][mad.ma_index]

            table = self.backend.symbols.element_table(td, self.ir.entitytype, mt.restriction)

            assert td.ttype != "zeros"

            if td.ttype == "ones":
                arg_factor = L.LiteralFloat(1.0)
            else:
                # Assuming B sparsity follows element table sparsity
                arg_factor = table[indices[i + 1]]
            arg_factors.append(arg_factor)
        return arg_factors

    def new_temp_symbol(self, basename):
        """Create a new code symbol named basename + running counter."""
        L = self.backend.language
        name = "%s%d" % (basename, self.symbol_counters[basename])
        self.symbol_counters[basename] += 1
        return L.Symbol(name)

    def get_var(self, num_points, v):
        if v._ufl_is_literal_:
            return self.backend.ufl_to_language.get(v)
        f = self.scope.get(v)
        return f

    def generate_partition(self, symbol, F, mode, num_points):
        L = self.backend.language

        definitions = []
        intermediates = []

        for i, attr in F.nodes.items():
            if attr['status'] != mode:
                continue
            v = attr['expression']
            mt = attr.get('mt')

            if v._ufl_is_literal_:
                vaccess = self.backend.ufl_to_language.get(v)
            elif mt is not None:
                # All finite element based terminals have table data, as well
                # as some, but not all, of the symbolic geometric terminals
                tabledata = attr.get('tr')

                # Backend specific modified terminal translation
                vaccess = self.backend.access.get(mt.terminal, mt, tabledata, num_points)
                vdef = self.backend.definitions.get(mt.terminal, mt, tabledata, num_points, vaccess)

                # Store definitions of terminals in list
                assert isinstance(vdef, list)
                definitions.extend(vdef)
            else:
                # Get previously visited operands
                vops = [self.get_var(num_points, op) for op in v.ufl_operands]

                # get parent operand
                pid = F.in_edges[i][0] if F.in_edges[i] else -1
                if pid and pid > i:
                    parent_exp = F.nodes.get(pid)['expression']
                else:
                    parent_exp = None

                # Mapping UFL operator to target language
                self._ufl_names.add(v._ufl_handler_name_)
                vexpr = self.backend.ufl_to_language.get(v, *vops)

                # Create a new intermediate for each subexpression
                # except boolean conditions and its childs
                if isinstance(parent_exp, ufl.classes.Condition):
                    # Skip intermediates for 'x' and 'y' in x<y
                    # Avoid the creation of complex valued intermediates
                    vaccess = vexpr
                elif isinstance(v, ufl.classes.Condition):
                    # Inline the conditions x < y, condition values
                    # This removes the need to handle boolean intermediate variables.
                    # With tensor-valued conditionals it may not be optimal but we
                    # let the compiler take responsibility for optimizing those cases.
                    vaccess = vexpr
                elif any(op._ufl_is_literal_ for op in v.ufl_operands):
                    # Skip intermediates for e.g. -2.0*x,
                    # resulting in lines like z = y + -2.0*x
                    vaccess = vexpr
                else:
                    # Record assignment of vexpr to intermediate variable
                    j = len(intermediates)
                    if self.ir.params["use_symbol_array"]:
                        vaccess = symbol[j]
                        intermediates.append(L.Assign(vaccess, vexpr))
                    else:
                        vaccess = L.Symbol("%s_%d" % (symbol.name, j))
                        intermediates.append(L.VariableDecl("const ufc_scalar_t", vaccess, vexpr))

            # Store access node for future reference
            self.scope[v] = vaccess

        # Join terminal computation, array of intermediate expressions,
        # and intermediate computations
        parts = []
        if definitions:
            parts += definitions
        if intermediates:
            if self.ir.params["use_symbol_array"]:
                alignas = self.ir.params["alignas"]
                parts += [L.ArrayDecl("ufc_scalar_t", symbol, len(intermediates), alignas=alignas)]
            parts += intermediates
        return parts

    def generate_original_coefficient_positions(self):
        L = self.backend.language
        num_coeffs = len(self.ir.original_coefficient_positions)
        orig_pos = L.Symbol("original_coefficient_positions")
        if num_coeffs > 0:
            parts = [L.ArrayDecl("static const int", orig_pos,
                                 values=self.ir.original_coefficient_positions,
                                 sizes=(num_coeffs, ))]
            parts += [L.Assign("expression->original_coefficient_positions", orig_pos)]
        else:
            parts = []
        return L.StatementList(parts)

    def generate_points(self):
        L = self.backend.language
        parts = L.ArrayDecl("static const double", "points", values=self.ir.points,
                            sizes=self.ir.points.shape)
        return parts

    def generate_value_shape(self):
        L = self.backend.language
        parts = L.ArrayDecl("static const int", "value_shape", values=self.ir.expression_shape,
                            sizes=len(self.ir.expression_shape))
        return parts
